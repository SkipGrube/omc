import java.util.*;


/* 
Notes about work:
-boy I need to look up a lot of simple Java stuff.  converting strings <-> arrays, .length vs. .size(), etc.
-started by going through entire array/list and swapping characters.  That was wrong lol - end up with what you started with
-very start:  the way the examples work, I assumed sort or reverse-sort was needed

-no points for imagination : this is pretty dirty and straightforward for-loops. 
There is probably a more "elegant" way to do it (thinking about recursion, but it's awkward with list vs. array data types)
*/

public class testList {
  /**
   * Finish the following method to conform to the @return documentation
   * 
   * @param items
   *          list of strings
   * @return A reversed list, where every string in the list is also reversed
   * 
   * Example
   * ["ab", "bc", "cd"] -> ["dc", "cb", "ba"]
   * 
   */
  public List<String> reverseAll(List<String> items) {
       
   
    int startCnt = 0;
    int endCnt = 0;
    char[] tmpStr;
    char tmpChar;

    String tmpStr2 = new String();

   
    // Task 1: Reverse each individual string in the list
    
    // outer loop: go through each string in the list, get a charArray of that string item
    for (int i=0; i < items.size(); i++) {
      
      tmpStr = items.get(i).toCharArray();
      
            
      // inner loop: go through the charArray of string, swap the characters
      // (startCnt goes 0 -> charArray / 2, endCnt goes charArrayLength -> charArray / 2 )
      // we only go halfway through, because doing the 2nd half means we reverse again, and end up where we started!
      //
      // (This bit could easily go in its own method:  "reverseString" or something like that)
      endCnt = tmpStr.length - 1;
      
      for (startCnt = 0; startCnt < tmpStr.length / 2; startCnt++) {
        
        tmpChar = tmpStr[startCnt];
        tmpStr[startCnt] = tmpStr[endCnt];
        tmpStr[endCnt] = tmpChar;
        
        endCnt--;
      }

      // finally, set our "official" string item to the swapped value:
      items.set(i, String.valueOf(tmpStr));

      
    }


    

    // Task 2: Swap each member of the list itself
    // Similar to inner loop above, but now we are swapping members of the list "items" instead of chars in an Array:
    endCnt = items.size() - 1;

    for (startCnt = 0; startCnt < items.size() / 2; startCnt++) {
      
      tmpStr2 = items.get(startCnt);
      items.set(startCnt, items.get(endCnt));
      items.set(endCnt, tmpStr2);

      endCnt--;
    
    }

  // return the double-reversed list
  return items;



  // Alternate idea: could be "clever" and do this with recursion, but would have to somehow convert a passed string to a "list" of characters
  // (instead of a simpler charArray).  Not worth it - the for loops are a little easier to understand
  }



  public static void main(String[] args) {
    
    List<String> myList = new ArrayList<String>() {{
      add("the");
      add("quick");
      add("brown");
      add("fox");
    }};

    
    System.out.println("Original List ::  " + myList.toString());

    testList n = new testList();
    System.out.println("Reversed List ::  " + n.reverseAll(myList).toString() );



  }

}
