#!/bin/bash

# The command:
#  ps guax  |   sort -nk4   |   tail -n1   |   awk '{ print $2 }'



#
# Blind first look, without reading docs::  
# (these are my thoughts looking at it for 10 secs., I'm just typing them out here):
#
# list all processes -> 
# sort them numerically... with something (does -k specify field separation? not familiar with it. Have to "man sort")  ->
# get the last entry ->
# print the 2nd field (default space separator) (pretty sure 2nd field is the PID, just off of reflex memory)
#
#
# Spitballing first impression::
# I imagine this gets the PID with the... highest CPU or memory usage?  Seems like the only things worth sorting in that output



#

# Trying it on my desktop and reading man pages:
#
# -my default "ps aux" that I use is equivalent to "ps gaux" -> "g" is deprecated now
#
# -yep, highest memory usage.  -k4 = 4th field == %MEM
#
# -have done something like this to a runaway process on my desktop, piped to "xargs -n 1 kill -s 9 "  lol.  "Oops"
